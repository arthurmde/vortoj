# About Vortoj
Vortoj is a very simple IRC Server implemented over [RFC 1459](https://tools.ietf.org/html/rfc1459#section-1.3.1) that has been developed during a Computer Network Programming subject at University of São Paulo (USP).

* **Professor**: Daniel Batista
* **University**: [University of São Paulo (USP)](http://www.usp.br)
* **Institute**: [Institute of Mathematics and Statistics (IME)](http://www.ime.usp.br)

You can find the latest version and the source code in our git repository at https://gitlab.com/arthurmde/vortoj

# Compile and Run

In order to properly use Vortoj IRC Server you need to write the correct parameters inside vortoj.config file. This file must have two lines, as one can see in the following example:

```
PORT_NUMBER
MAXIMUM_NUMBER_OF_CLIENTS
```

These parameters are going to be used as arguments while running the server.

After this, you can use the makefile to compile and run the server or, if you want, only compile the source code:

* To compile: `$ make`
* To run: `$ make run`
* To check server conectivity: `$ make check`

# Technical References

All technical references can be found into the following files:

* **[irc-rfc-compilation.md](irc-rfc-compilation.md)**: it's a compilation of RFC 1459 with the most important things related to this work.

# Authors

* Arthur de Moura Del Esposte <arthurmde at gmail dot com || esposte at ime dot usp dot br>
* Athos Coimbra Ribeiro <athoscr at ime dot usp dot br>

# License

Copyright © 2015 The Author developers.

This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License along with this program. If not, see www.gnu.org/licenses/
