#include "channel.h"
#include "list.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

Channel* create_channel(char* name, char* topic)
{
  Channel* channel = calloc(1, sizeof(Channel));
  channel->name = name;
  channel->topic = topic;
  channel->users = create_list();
  initialize_list(channel->users);

  return channel;
}

int free_channel(Channel* channel)
{
  if(!channel)
    return -1;

  free_list(channel->users);

  free(channel);

  return 0;
}

int add_user(Channel *channel, Client *client)
{
  return push_back(channel->users, client);
}

int remove_user(Channel *channel, Client *client)
{
  if(!channel)
    return -1;

  Node* temp;
  int i = 0;
  int found = -1;

  for(temp = channel->users->head; temp; temp = temp->next, i++)
  {
    Client* current_client = (Client*) temp->data;

    if(strcmp(current_client->nick, client->nick) == 0)
    {
      found = i;
      temp = channel->users->tail;
    }
  }

  if(found == -1)
    return -2;

  return pop(channel->users, found);
}

Channel* find_channel(List* list, char* channel_name)
{
  if(!list)
    return NULL;

  Node* temp;

  for(temp = list->head; temp; temp = temp->next)
  {
    Channel* channel = (Channel*) temp->data;

    if(strcmp(channel->name, channel_name) == 0)
      return channel;
  }

  return NULL;
}

char* list_channel_users(Channel* channel)
{
  char* users_list = calloc(4096, sizeof(char));
  if(!channel)
    return NULL;

  Node* temp;

  for(temp = channel->users->head; temp; temp = temp->next)
  {
    Client* client = (Client*) temp->data;
    strcat(users_list,client->nick);
    strcat(users_list," ");
  }
  users_list[strlen(users_list)-1] = '\0';

  return users_list;
}
