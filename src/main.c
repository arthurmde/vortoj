#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <stdbool.h>
#include <time.h>
#include <netdb.h>
#include <pthread.h>
#include <netinet/in.h>
#include <unistd.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <arpa/inet.h>

#include "network.h"
#include "irc.h"
#include "client.h"


/*Configuration paramaters*/
#define MAXLINE 4096
#define PORT_PARAMATER_ORDER 1
#define CLIENTS_PARAMATER_ORDER 2

void verify_arguments(int argc, char** argv);
void *protocol(void *data);
void create_client_thread(pthread_t *clients, int client_id, int connected_clients, Server *irc_server);

typedef struct _ThreadData
{
  int client_id;
  int number;
  Server* server;
}ThreadData;

int main(int argc, char** argv)
{
  struct sockaddr_in server;
  char send_buffer[MAXLINE+1];

  verify_arguments(argc, argv);
  welcome_message();

  int port_number = atoi(argv[PORT_PARAMATER_ORDER]);
  maximum_number_of_clients = atoi(argv[CLIENTS_PARAMATER_ORDER]);

  /* TODO: Create connection_ids as Linked List */
  int connection_ids[maximum_number_of_clients];
  pthread_t clients[maximum_number_of_clients];
  memset(connection_ids, '0', sizeof(connection_ids));
  int listen_id = create_socket(&server, send_buffer);

  configure_server_socket(&server, listen_id, port_number);
  listen_connections(listen_id, maximum_number_of_clients);

  Server *irc_server = create_server("Vortoj");
  print_channels(irc_server);

  printf("==> Servidor no ar. Aguardando conexoes na porta %d <==\n", port_number);
  printf("[Para finalizar, pressione CTRL+c ou rode um kill ou killall]\n");

  int connected_clients = 0;
  while(true)
  {
    connection_ids[connected_clients] = get_connection(listen_id);

    create_client_thread(clients, connection_ids[connected_clients], connected_clients+1, irc_server);

    connected_clients++;

    printf("\n%d conexoes abertas\n", connected_clients);
  }

  for(int i = 0; i < maximum_number_of_clients; i++)
    pthread_join(clients[i],NULL);

  free_server(irc_server);

  return EXIT_SUCCESS;
}

void verify_arguments(int argc, char** argv)
{
  if(argc != 3)
  {
    fprintf(stderr, "Error: need exactly 1 argument. Usage:\n  $ server <port_number> <maximum_number_of_clients>\n");
    exit(EXIT_FAILURE);
  }
}

void *protocol(void *data)
{
  ThreadData *client_data = (ThreadData *) data;
  Client *client = create_client();
  int line;
  char receive_buffer[MAXLINE+1];

  client->id = client_data->client_id;

  while((line = read(client->id, receive_buffer, MAXLINE)) > 0)
  {
    receive_buffer[line] = 0;

    printf("\n>>> Cliente conectado na posicao %d enviou: ", client_data->number);

    char* split_msg = NULL;
    char *msg_ptr = NULL;
    split_msg = strtok_r(receive_buffer, "\r\n", &msg_ptr);
    do {
      if(!split_msg)
        break;
      char *client_msg = calloc(strlen(split_msg)+3,sizeof(char));
      strcpy(client_msg, split_msg);
      strcat(client_msg, "\r\n");
      /* DEBUG HANDLED MSG */
      printf("\n[%s]\n", client_msg);
      handle_action(client_msg, client, client_data->server);
      free(client_msg);
    } while((split_msg = strtok_r(NULL, "\r\n", &msg_ptr)));

    if(client->quit)
      break;

    memset(receive_buffer, 0, MAXLINE);
  }

  printf("\n>>> Cliente conectado na posicao %d enviou está saindo!", client_data->number);
  close(client_data->client_id);

  free_client(client);

  return 0;
}

void create_client_thread(pthread_t *clients, int client_id, int connected_clients, Server* irc_server)
{
  ThreadData *data = calloc(1, sizeof(ThreadData));
  data->client_id = client_id;
  data->number = connected_clients;
  data->server = irc_server;
  pthread_create(&(clients[connected_clients]), NULL, protocol, (void *) data);
}
