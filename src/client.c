#include "client.h"
#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>

#define OK 0

Client *create_client()
{
  Client* client = calloc(1, sizeof(Client));

  initialize_client(client);

  return client;
}

void initialize_client(Client* client)
{
  client->id = -1;
  client->nick = NULL;
  client->name = NULL;
  client->username = NULL;
  client->password = NULL;
  client->is_registered = false;
  client->quit = false;
}

int free_client(Client *client)
{
  if(client->nick)
  {
    free(client->nick);
  }

  if(client->name)
  {
    free(client->name);
  }

  if(client->username)
  {
    free(client->username);
  }

  free(client);

  return OK;
}
