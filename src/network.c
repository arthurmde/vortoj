#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <stdbool.h>
#include <time.h>
#include <netdb.h>
#include <netinet/in.h>
#include <unistd.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <arpa/inet.h>

#include "network.h"


int create_socket(struct sockaddr_in* server, char* send_buffer)
{
  int listen_id;

  memset(server, '0', sizeof(*server));
  memset(send_buffer, '0', sizeof(*send_buffer));

  listen_id = socket(AF_INET, SOCK_STREAM, 0);
  verify_socket_creation(listen_id);

  return listen_id;
}

void verify_socket_creation(int listen_id)
{
  if(listen_id == -1)
  {
    fprintf(stderr, "Error: could not create server socket.");
    exit(SOCKET_FAILURE);
  }
}

void configure_server_socket(struct sockaddr_in* server, int listen_id, int port_number)
{
  server->sin_family = AF_INET;
  server->sin_addr.s_addr = htonl(INADDR_ANY);
  server->sin_port = htons(port_number);

  verify_socket_bind(server, listen_id);
}

void verify_socket_bind(struct sockaddr_in* server, int listen_id)
{
  int result = bind(listen_id, (struct sockaddr *) server, sizeof(*server));
  if(result == -1)
  {
    fprintf(stderr, "Error: could not bind on the Server.");
    exit(BIND_FAILURE);
  }
}

void listen_connections(int listen_id, int maximum_number_of_clients)
{
  int result = listen(listen_id, maximum_number_of_clients);
  if (result == -1)
  {
    fprintf(stderr, "Error: the server socket could not enter in listen mode.");
    exit(LISTEN_FAILURE);
  }
}

int get_connection(int listen_id)
{
  int result = accept(listen_id, (struct sockaddr *) NULL, NULL);
  if (result == -1 )
  {
    fprintf(stderr, "Error: could not accept a client connection.");
    exit(CONNECT_FAILURE);
  }

  return result;
}
