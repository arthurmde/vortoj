#include "node.h"
#include <stdlib.h>
#include <stdio.h>

Node * create_node()
{
	return calloc(1, sizeof(Node));
}

int free_node(Node *n)
{
	if(!n)
		return -1;

	free(n);

	return 0;
}
