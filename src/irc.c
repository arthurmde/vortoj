#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <unistd.h>
#include <time.h>
#include "irc.h"
#include "commands.h"
#include "client.h"
#include "replies.h"
#include "weather.h"
#include "node.h"

#define MAXLINE 4096

Server* create_server(char* name)
{
  Server *server = calloc(1, sizeof(Server));
  if(!server)
    return NULL;

  server->name = name;
  server->channels = create_list();
  initialize_list(server->channels);

  create_new_channel(server, "#ccsl", "dúvidas? simplesmente pergunte -- não precisa perguntar se pode perguntar =) || http://ccsl.ime.usp.br/");
  create_new_channel(server, "#ime", "Bem vindo ao IME - http://ime.usp.br/");

  server->clients = calloc(maximum_number_of_clients,sizeof(Client*));

  return server;
}

int free_server(Server* server)
{
  if(!server)
    return -1;

  free_list(server->channels);

  free(server);

  return 0;
}

void welcome_message()
{
  printf("\n");
  printf("=== Welcome to Vortoj IRC Server ===\n");
  printf("\n");
  printf("Developed by:\n");
  printf("- Arthur Del Esposte\n");
  printf("- Athos Ribeiro\n");
  printf("\n");
}

void handle_action(char *receive_buffer, Client* client, Server* server)
{
  int size = strlen(receive_buffer);

  if(!verify_irc_message(receive_buffer, size))
    return;

  /* TODO: Had to double array size since it was being overran, check this asap */
  char *command = calloc(2, size);
  strcpy(command, receive_buffer);

  remove_special_chars(command, size);

  char* command_name = strtok(command, " ");
  if(!command_name)
    return;

  /* Check if client is registered */
  if(!(client->is_registered) && !(equal(command_name, QUIT_CMD) || equal(command_name, NICK_CMD) || equal(command_name, USER_CMD) || equal(command_name, PASS_CMD)))
  {
    /*TODO: fix error messages */
    char err_notregistered_str[512];
    sprintf(err_notregistered_str, replies[ERR_NOTREGISTERED], "Vortoj", "");
    write(client->id, err_notregistered_str, strlen(err_notregistered_str));
    return;
  }

  if(equal(command_name, QUIT_CMD))
    quit(receive_buffer, client, server);
  else if (equal(command_name, NICK_CMD) || equal(command_name, USER_CMD) || equal(command_name, PASS_CMD))
    registration(command_name, command, client, server);
  else if (equal(command_name, JOIN_CMD))
    join(server, command_name, client);
  else if (equal(command_name, PART_CMD))
    part(server, command_name, client);
  else if (equal(command_name, LIST_CMD))
    list(server, command_name, client);
  else if (equal(command_name, MACDATA_CMD))
    macdata(command, client);
  else if (equal(command_name, MACHORA_CMD))
    machora(command, client);
  else if (equal(command_name, MACTEMPERATURA_CMD))
    mactemperatura(command, client);
  else if (equal(command_name, PRIVMSG_CMD))
    privmsg(server, command, client);
  else
  {
    unknown_command(command_name, client);
  }

  free(command);
}

void send_quit_message(Client* client, char* message)
{

}

void quit(char *command, Client* client, Server* server)
{
  char *close_message = "Quiting IRC server...\n\0";
  client->quit = true;

  write(client->id, close_message, strlen(close_message));
  // send_quit_message(client, quit_message);
  for(int i=0;i<=maximum_number_of_clients;i++)
  {
    if((server->clients[i]) && (equal(server->clients[i]->nick,client->nick)))
    {
      server->clients[i] = NULL;
      break;
    }
  }
}

bool equal(const char* first, const char* second)
{
  if(strcmp(first, second) == 0)
    return true;

  return false;
}

void registration(char* command_name, char* command, Client *client, Server* server)
{
  char* nick = NULL;
  char* username = NULL;
  /*char* hostname = NULL;*/
  /*char* servername = NULL;*/
  char* realname = NULL;

  if(equal(command_name, NICK_CMD))
  {
    /*TODO: handke nick size/empty*/
    char* tmp_nick = client->nick;
    nick = strtok(NULL, " ");
    if(!nick)
    {
      char error[MAXLINE];
      sprintf(error, replies[ERR_NEEDMOREPARAMS], "Vortoj", "", "NICK");
      write(client->id, error, strlen(error));
      return;
    }

    client->nick = strdup(nick);
    if(client->is_registered)
    {
      char nick_change_msg[512];
      sprintf(nick_change_msg, ":%s NICK :%s\r\n", tmp_nick, client->nick);
      /* TODO: must send this to all users who share channel with this client */
      write(client->id, nick_change_msg, strlen(nick_change_msg));
    }
  }

  else if(equal(command_name, USER_CMD))
  {
    /*TODO: handke last parm with : */
    if(client->is_registered)
    {
      char err_alreadyregistred_str[512];
      sprintf(err_alreadyregistred_str, replies[ERR_ALREADYREGISTRED], "Vortoj", client->nick);
      write(client->id, err_alreadyregistred_str, strlen(err_alreadyregistred_str));
      return;
    }
    /* servername and hostname are not used by clients */
    username = strtok(NULL, " ");
    /*commented those out to supress warnings*/
    /*hostname = strtok(NULL, " ");*/
    /*servername = strtok(NULL, " ");*/
    strtok(NULL, " ");
    strtok(NULL, " ");
    realname = strtok(NULL, "\n");
    if(!username || !realname)
    {
      char error[MAXLINE];
      sprintf(error, replies[ERR_NEEDMOREPARAMS], "Vortoj", "", "USER");
      write(client->id, error, strlen(error));
      return;
    }

    if(realname[0] == ':')
      realname+=1;
    else
      realname = strtok(realname, " ");

    client->name = strdup(realname);
    client->username = strdup(username);
  }

  if(client->nick && client->name && client->username)
  {
    if(client->is_registered)
      return;

    for(int i=0;i<=maximum_number_of_clients;i++)
    {
      if(!(server->clients[i]))
      {
        server->clients[i] = client;
        client->is_registered=true;
        break;
      }
    }

    if(!(client->is_registered))
    {
      printf("COULD NOT REGISTER\r\n");
      return;
    }

    char rpl_welcome_str[512];
    char rpl_yourhost_str[512];
    char rpl_created_str[512];
    char rpl_myinfo_str[512];

    sprintf(rpl_welcome_str, replies[RPL_WELCOME], "Vortoj", client->nick, "Vortoj", client->nick, client->username, "localhost");
    sprintf(rpl_yourhost_str, replies[RPL_YOURHOST], "Vortoj", client->nick, "Vortoj", "vortoj-0.1");
    /* TODO: get proper date */
    sprintf(rpl_created_str, replies[RPL_CREATED], "Vortoj", client->nick, "on Sep. 10th");
    sprintf(rpl_myinfo_str, replies[RPL_MYINFO], "Vortoj", client->nick, "Vortoj", "vortoj-0.1", "", "");
    write(client->id, rpl_welcome_str, strlen(rpl_welcome_str));
    write(client->id, rpl_yourhost_str, strlen(rpl_yourhost_str));
    write(client->id, rpl_created_str, strlen(rpl_created_str));
    write(client->id, rpl_myinfo_str, strlen(rpl_myinfo_str));
  }
}

void join(Server* server, char* command_name, Client *client)
{
  if(!server || !client)
    return;

  char* channel_name = NULL;

  channel_name = strtok(NULL, " ");

  Channel* channel = find_channel(server->channels, channel_name);

  if(!channel)
  {
    char err_nosuchchannel_str[512];
    sprintf(err_nosuchchannel_str, replies[ERR_NOSUCHCHANNEL], "Vortoj", client->nick, channel_name);
    write(client->id, err_nosuchchannel_str, strlen(err_nosuchchannel_str));
    return;
  }

  char* check_nick_list = list_channel_users(channel);
  char* check_nick = strtok(check_nick_list, " ");
  do
  {
    if(!check_nick)
      break;

    if(equal(check_nick, client->nick))
    {
      printf("ALREADY IN THE CHANNEL\n");
      return;
    }
  } while((check_nick = strtok(NULL, " ")));
  free(check_nick_list);

  add_user(channel, client);

  /* this msg must be broadcasted to all members in channel */
  char join_broadcast[MAXLINE];
  sprintf(join_broadcast, ":%s JOIN :%s\r\n", client->nick, channel->name);
  Node* temp;
  for(temp = channel->users->head; temp; temp = temp->next)
  {
    Client* current = (Client*) temp->data;
    write(current->id, join_broadcast, strlen(join_broadcast));
  }



  char welcome[MAXLINE];
  sprintf(welcome, replies[RPL_TOPIC], "Vortoj", client->nick, channel->name, channel->topic);
  write(client->id, welcome, strlen(welcome));

  char names[MAXLINE];
  sprintf(names, replies[RPL_NAMREPLY], "Vortoj", client->nick, channel->name, list_channel_users(channel));
  write(client->id, names, strlen(names));

  char end_of_names[MAXLINE];
  sprintf(end_of_names, replies[RPL_ENDOFNAMES], "Vortoj", client->nick, channel->name);
  write(client->id, end_of_names, strlen(end_of_names));
}

void part(Server* server, char* command_name, Client *client)
{
  if(!server || !client)
    return;

  char* channel_name = NULL;

  channel_name = strtok(NULL, " ");

  if(!channel_name)
  {
    char error[MAXLINE];
    sprintf(error, replies[ERR_NEEDMOREPARAMS], "Vortoj", client->nick, "PART");
    write(client->id, error, strlen(error));
    return;
  }

  Channel* channel = find_channel(server->channels, channel_name);

  if(!channel)
  {
    char error[MAXLINE];
    sprintf(error, replies[ERR_NOSUCHCHANNEL], "Vortoj", client->nick, "PART");
    write(client->id, error, strlen(error));
    return;
  }

  if(remove_user(channel, client) != OK)
  {
    char error[MAXLINE];
    sprintf(error, replies[ERR_NOTONCHANNEL], "Vortoj", client->nick, channel->name, "PART");
    write(client->id, error, strlen(error));
    return;
  }

  char goodbye[MAXLINE];
  sprintf(goodbye, ":%s PART :%s\r\n", client->nick, channel->name);

  write(client->id, goodbye, strlen(goodbye));

  Node* temp;
  for(temp = channel->users->head; temp; temp = temp->next)
  {
    Client* current = (Client*) temp->data;
    write(current->id, goodbye, strlen(goodbye));
  }
}

void list(Server* server, char* command_name, Client* client)
{
  Node* temp;
  char list_start[MAXLINE];
  sprintf(list_start, replies[RPL_LISTSTART], "Vortoj", client->nick);

  write(client->id, list_start, strlen(list_start));

  for(temp = server->channels->head; temp; temp = temp->next)
  {
    char response[MAXLINE];
    Channel *channel = (Channel *) temp->data;
    char number_of_users[10];
    snprintf(number_of_users, 10, "%d", size_list(channel->users));

    sprintf(response, replies[RPL_LIST], "Vortoj", client->nick, channel->name, number_of_users, channel->topic);

    write(client->id, response, strlen(response));
  }

  char end_list[MAXLINE];
  sprintf(end_list, replies[RPL_LISTEND], "Vortoj", client->nick);
  write(client->id, end_list, strlen(end_list));
}

int create_new_channel(Server* server, char* name, char* topic)
{
  if(!server || !name)
    return -1;

  Channel* channel = create_channel(name, topic);
  if(!channel)
    return -2;

  return push_front(server->channels, channel);
}

int print_channels(Server* server)
{
  if(!server)
    return -1;

  printf("\n== Canais Disponíveis no Servidor %s==\n\n", server->name);

  Node* temp;

  for(temp = server->channels->head; temp; temp = temp->next)
  {
    Channel *channel = (Channel *) temp->data;
    printf("%s\n", channel->name);
  }

  printf("\n");

  return 0;
}

bool verify_irc_message(char* receive_buffer, int size)
{
  if(size == 2)
    return false;

  const int LF = 10;
  const int CR = 13;

  if((int)receive_buffer[size-1] == LF && (int)receive_buffer[size-2] == CR)
    return true;

  return false;
}

void remove_special_chars(char* receive_buffer, int size)
{
  receive_buffer[size-2] = '\0';
}

void macdata(char* command, Client* client)
{
  char server_date[512];
  time_t t = time(NULL);
  struct tm tm = *localtime(&t);
  sprintf(server_date, ":Vortoj 900 %s :%02d/%02d/%d\r\n",client->nick, tm.tm_mday, tm.tm_mon+1, tm.tm_year+1900);
  write(client->id, server_date, strlen(server_date));
}

void machora(char* command, Client* client)
{
  char server_time[512];
  time_t t = time(NULL);
  struct tm tm = *localtime(&t);
  sprintf(server_time, ":Vortoj 901 %s :%02d:%02d:%02d-%s\r\n",client->nick, tm.tm_hour, tm.tm_min, tm.tm_sec, tzname[0]);
  write(client->id, server_time, strlen(server_time));
}

void mactemperatura(char* command, Client* client)
{
  int temperature = 0;
  char weather_xml[4096];
  char mac_output[512];
  get_raw_weather(weather_xml);
  temperature = parse_temperature(weather_xml);
  sprintf(mac_output,":Vortoj 902 %s :Temperatura em SP: %d oC\r\n",client->nick,temperature);
  write(client->id, mac_output, strlen(mac_output));
}

void unknown_command(char* command_name, Client* client)
{
  char err_unknowncommand_str[512];
  sprintf(err_unknowncommand_str, replies[ERR_UNKNOWNCOMMAND], "Vortoj", client->nick, command_name);
  write(client->id, err_unknowncommand_str, strlen(err_unknowncommand_str));
}

void privmsg(Server* server, char* command, Client* client)
{
  char* message;
  char* dest_str = strtok(NULL, " ");
  Client* dest_client = NULL;
  Channel* dest_channel = NULL;
  if(!(dest_str) || dest_str[0] == ':')
  {
    char err_needmoreparams_str[512];
    sprintf(err_needmoreparams_str, replies[ERR_NEEDMOREPARAMS], "Vortoj", client->nick, command);
    write(client->id, err_needmoreparams_str, strlen(err_needmoreparams_str));
    return;
  }

  if(dest_str[0] == '#')
  {

  }

  if(!(dest_client = get_user_by_nick(server, dest_str)) && !(dest_channel = find_channel(server->channels, dest_str)))
  {
    char err_nosuchnick_str[512];
    sprintf(err_nosuchnick_str, replies[ERR_NOSUCHNICK], "Vortoj", client->nick, dest_str);
    write(client->id, err_nosuchnick_str, strlen(err_nosuchnick_str));
    return;
  }

  message = strtok(NULL, "\n");
  if(!(message))
  {
    char err_needmoreparams_str[512];
    sprintf(err_needmoreparams_str, replies[ERR_NEEDMOREPARAMS], "Vortoj", client->nick, command);
    write(client->id, err_needmoreparams_str, strlen(err_needmoreparams_str));
    return;
  }

  if(message[0] == ':')
    message+=1;

  else
    message = strtok(message, " ");

  char private_message[512];
  if(dest_client)
  {
    sprintf(private_message, ":%s PRIVMSG %s :%s\r\n", client->nick, dest_client->nick, message);
    write(dest_client->id, private_message, strlen(private_message));
  }

  else if(dest_channel)
  {
    sprintf(private_message, ":%s PRIVMSG %s :%s\r\n", client->nick, dest_channel->name, message);
    Node* temp;
    for(temp = dest_channel->users->head; temp; temp = temp->next)
    {
      Client* current = (Client*) temp->data;
      if(current == client)
        continue;

      write(current->id, private_message, strlen(private_message));
    }
  }

}

Client* get_user_by_nick(Server* server, char* nick)
{
  for(int i=0;i<=maximum_number_of_clients;i++)
  {
    if(!(server->clients[i]))
    {
      continue;
    }
    if(equal(server->clients[i]->nick,nick))
    {
      return server->clients[i];
    }
  }
  return NULL;
}
