#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include "weather.h"

int get_raw_weather(char* server_reply)
{
  int sockfd;
  struct sockaddr_in server;

  if((sockfd = socket(AF_INET, SOCK_STREAM, 0)) < 0 )
  {
    perror("error");
    exit(errno);
  }

  server.sin_addr.s_addr = inet_addr("150.163.141.175");
  server.sin_family = AF_INET;
  server.sin_port = htons(80);
  if (connect(sockfd , (struct sockaddr *)&server , sizeof(server)) < 0)
  {
    perror("connect failed. Error");
    return 1;
  }

  char* message = "GET /XML/estacao/SBSP/condicoesAtuais.xml HTTP/1.1\r\nHost: servicos.cptec.inpe.br\r\n\r\n";
  if( send(sockfd, message , strlen(message) , 0) < 0)
  {
    puts("Send failed");
    return 1;
  }

  if( recv(sockfd , server_reply , 2000 , 0) < 0)
  {
    puts("recv failed");
  }

  close(sockfd);
  return 0;
}

int parse_temperature(char *weather_xml)
{
  int temperature = 0;
  char *temperature_str = malloc(3*sizeof(char));
  /*TODO: parce the embedded xml in a better way*/
  temperature_str[0] = weather_xml[302];
  temperature_str[1] = weather_xml[303];
  temperature_str[2] = '\0';
  temperature = atoi(temperature_str);
  free(temperature_str);
  return temperature;
}
