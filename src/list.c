#include <stdio.h>
#include "list.h"
#include <stdlib.h>
#include "node.h"



List *create_list()
{
	return calloc(1, sizeof(List));
}

int free_list(List *list)
{
	Node *temp = NULL, *n = NULL;
	if(!list)
		return -1;

	temp = list->head;
	while(temp)
	{
		n = temp;
		temp = temp->next;
		free_node(n);
	}

	free(list);

	return 0;
}

int initialize_list(List *list)
{
	if(!list)
		return -1;

	list->head = NULL;
	list->tail = NULL;
	return 0;
}

int empty_list(List *list)
{
	if(!list)
		return -1;

	if(!list->head)
		return 0;
	else
		return 1;
}

int push_front(List *list, void *data)
{
	if(!list || !data)
		return -1;

	Node *temp = create_node();

	if(!temp)
		return -2;

	temp->data = data;
	temp->next = list->head;
	temp->prev = NULL;

	if(!list->head)
		list->tail = temp;
	else
		list->head->prev = temp;

	list->head=temp;

	return 0;
}

int pop_back(List *list)
{
	if(!list)
		return ERROR_NULL_PARAMETER;

	Node *temp = list->tail;

	if(!temp)
		return ERROR_EMPTY_LIST;

	list->tail=temp->prev;

	if(temp==list->head)
		list->head=NULL;
	else
		list->tail->next=NULL;

	free_node(temp);

	return OK;
}

int push_back(List *list, void *data)
{
	if(!list || !data)
		return ERROR_NULL_PARAMETER;

	Node *temp = create_node();

	if(!temp)
		return ERROR_OUT_OF_MEMORY;

	temp->data = data;
	temp->prev = list->head;
	temp->next = NULL;

	if(!list->head)
		list->head = temp;
	else
		list->tail->next = temp;

	list->tail=temp;

	return OK;
}

int pop_front(List *list)
{
	if(!list)
		return ERROR_NULL_PARAMETER;

	Node *temp = list->head;

	if(!temp)
		return ERROR_EMPTY_LIST;

	list->head=temp->next;

	if(temp==list->tail)
		list->tail=NULL;
	else
		list->head->prev=NULL;

	free_node(temp);

	return OK;
}

List *clone_list(List* list)
{
	if(!list)
		return NULL;
	List *clone=NULL;
	Node *temp=NULL;
	int rc;
	if(!list)
		return NULL;

	clone = create_list();

	if(!clone)
		return NULL;

	for(temp=list->head; temp; temp=temp->next)
	{
		rc=push_back(clone, temp->data);

		if(rc)
		{
			free_list(clone);
			return NULL;
		}

	}

	return clone;
}

int size_list(List *list)
{
	int size = 0;
	Node *node = NULL;
	if(!list)
		return ERROR_NULL_PARAMETER;

	for(size = 0, node=list->head; node; node = node->next, size++);

	return size;
}

int element_list(void *element, List *list, int position)
{
	if(!element || !list)
		return ERROR_NULL_PARAMETER;
	if((position < 0) || (position > size_list(list)))
		return ERROR_INVALID_PARAMETER;

	int i;
	Node *node = NULL;

	for(i=1, node = list->head; i < position; i++, node=node->next);
		element = node->data;

	return OK;
}

int push (List *list, void* data, int position)
{
	int size, i;
	Node  *node=NULL, *temp = NULL;

	if(!list)
		return ERROR_NULL_PARAMETER;

	size = size_list(list);

	if(position < 0 || position >= size)
		return ERROR_INVALID_PARAMETER;

	if(!position)
		return push_front(list, data);
	else if(position == size-1)
		return push_back(list, data);
	else
	{
		node = create_node();

		node->data = data;
		for(i=0, temp = list->head; i<position; i++, temp = temp->next);

		node->next = temp;
		node->prev = temp->prev;
		temp->prev->next = node;
		temp->prev = node;

	}

	return OK;
}

int pop(List *list, int position)
{
	int size, i;
	Node *temp = NULL;

	if(!list)
		return ERROR_NULL_PARAMETER;

	size = size_list(list);

	if(position < 0 || position >= size)
		return ERROR_INVALID_PARAMETER;

	if(!position)
		return pop_front(list);
	else if(position == size-1)
		return pop_back(list);
	else
	{
		for(i=0, temp = list->head; i<position; i++, temp = temp->next);


		temp->prev->next = temp->next;
		temp->next->prev = temp->prev;

		free_node(temp);
	}

	return OK;
}


int copy_list(List *copy, List *original)
{
	Node *temp = NULL;
	Node *temp2 = NULL;
	int size_c, size_o;

	if(!original || !copy)
		return ERROR_NULL_PARAMETER;

	size_c = size_list(copy);
	size_o = size_list(original);

	if(size_c == size_o)
	{
		for(temp = original -> head, temp2 = copy->head; temp; temp = temp->next, temp2 = temp2->next)
			temp2 ->data = temp->data;
	}else if(size_c < size_o)
	{
		for(temp = original -> head, temp2 = copy->head; temp2; temp = temp->next, temp2 = temp2->next)
			temp2 ->data = temp->data;
		for(; temp; temp = temp->next)
			push_back(copy, temp->data);

	} else
	{
		for(temp = original -> head, temp2 = copy->head; temp; temp = temp->next, temp2 = temp2->next)
			temp2 ->data = temp->data;
		for(int i =0; i < size_c - size_o; i++)
			pop_back(copy);
	}

	return OK;
}

int clear_list(List *list)
{
	if(!list)
		return ERROR_NULL_PARAMETER;

	while(!empty_list(list))
		pop_back(list);

	return OK;
}