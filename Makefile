FILES = src/*.c include/*.h
INPUT = src/*.c
FLAGS = -Iinclude -Wall -g -pthread -ansi -pedantic -std=gnu99 -lpthread
OUTPUT = server

CONFIG_FILE = vortoj.conf
PORT = `head -1 $(CONFIG_FILE) | tail -1`
MAXIMUM_NUMBER_OF_CLIENTS = `head -2 $(CONFIG_FILE) | tail -1`

all: compile

compile: $(FILES)
	gcc -o $(OUTPUT) $(FLAGS) $(INPUT)

run:
	./server $(SERVER) $(PORT) $(MAXIMUM_NUMBER_OF_CLIENTS)

check:
	./test/run.sh

clean:
	rm -f server
