#!/bin/bash

testSocket() {

  ./server 7000 10 > /dev/null &
  PID=$!
  assertEquals `nmap -Pn -p7000 localhost | grep '7000/tcp open' > /dev/null; echo $?` 0
  # disown avoids messages warning about process termination
  disown $PID
  kill -9 $PID

}

 . /usr/share/shunit2/shunit2

