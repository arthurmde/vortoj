#ifndef CLIENT_H
#define CLIENT_H

#include <stdbool.h>

typedef struct _Client
{
  int id;
  char *nick;
  char *name;
  char *username;
  char *password;
  bool is_registered;
  bool quit;
}Client;

extern Client *create_client();
extern void initialize_client(Client* client);
extern int free_client(Client *client);

#endif
