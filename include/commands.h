#ifndef COMMANDS_H
#define COMMANDS_H

const char* CONNECT_CMD = "CONNECT";
const char* QUIT_CMD = "QUIT";
const char* NICK_CMD = "NICK";
const char* USER_CMD = "USER";
const char* PASS_CMD = "PASS";
const char* PART_CMD = "PART";
const char* JOIN_CMD = "JOIN";
const char* LIST_CMD = "LIST";
const char* MACDATA_CMD = "MACDATA";
const char* MACHORA_CMD = "MACHORA";
const char* MACTEMPERATURA_CMD = "MACTEMPERATURA";
const char* PRIVMSG_CMD = "PRIVMSG";

#endif
