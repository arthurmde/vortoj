#ifndef CHANNEL_H
#define CHANNEL_H

#include "list.h"

typedef struct _Channel
{
  List* users;
  char* name;
  char* topic;
}Channel;

extern Channel* create_channel(char* name, char* topic);
extern int free_channel(Channel* channel);
extern int add_user(Channel *channel, Client *client);
extern int remove_user(Channel *channel, Client *client);
extern Channel* find_channel(List* list, char* channel_name);
extern char* list_channel_users(Channel* channel);

#endif
