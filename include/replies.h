#ifndef REPLIES_H
#define REPLIES_H

#define ERR_NOSUCHNICK 401
#define ERR_NOSUCHSERVER 402
#define ERR_NOSUCHCHANNEL 403
#define ERR_CANNOTSENDTOCHAN 404
#define ERR_TOOMANYCHANNELS 405
#define ERR_WASNOSUCHNICK 406
#define ERR_TOOMANYTARGETS 407
#define ERR_NOORIGIN 409
#define ERR_NORECIPIENT 411
#define ERR_NOTEXTTOSEND 412
#define ERR_NOTOPLEVEL 413
#define ERR_WILDTOPLEVEL 414
#define ERR_UNKNOWNCOMMAND 421
#define ERR_NOMOTD 422
#define ERR_NOADMININFO 423
#define ERR_FILEERROR 424
#define ERR_NONICKNAMEGIVEN 431
#define ERR_ERRONEUSNICKNAME 432
#define ERR_NICKNAMEINUSE 433
#define ERR_NICKCOLLISION 436
#define ERR_USERNOTINCHANNEL 441
#define ERR_NOTONCHANNEL 442
#define ERR_USERONCHANNEL 443
#define ERR_NOLOGIN 444
#define ERR_SUMMONDISABLED 445
#define ERR_USERSDISABLED 446
#define ERR_NOTREGISTERED 451
#define ERR_NEEDMOREPARAMS 461
#define ERR_ALREADYREGISTRED 462
#define ERR_NOPERMFORHOST 463
#define ERR_PASSWDMISMATCH 464
#define ERR_YOUREBANNEDCREEP 465
#define ERR_KEYSET 467
#define ERR_CHANNELISFULL 471
#define ERR_UNKNOWNMODE 472
#define ERR_INVITEONLYCHAN 473
#define ERR_BANNEDFROMCHAN 474
#define ERR_BADCHANNELKEY 475
#define ERR_NOPRIVILEGES 481
#define ERR_CHANOPRIVSNEEDED 482
#define ERR_CANTKILLSERVER 483
#define ERR_NOOPERHOST 491
#define ERR_UMODEUNKNOWNFLAG 501
#define ERR_USERSDONTMATCH 502
#define RPL_NONE 300
#define RPL_USERHOST 302
#define RPL_ISON 303
#define RPL_AWAY 301
#define RPL_UNAWAY 305
#define RPL_NOWAWAY 306
#define RPL_WHOISUSER 311
#define RPL_WHOISSERVER 312
#define RPL_WHOISOPERATOR 313
#define RPL_WHOISIDLE 317
#define RPL_ENDOFWHOIS 318
#define RPL_WHOISCHANNELS 319
#define RPL_WHOWASUSER 314
#define RPL_ENDOFWHOWAS 369
#define RPL_LISTSTART 321
#define RPL_LIST 322
#define RPL_LISTEND 323
#define RPL_CHANNELMODEIS 324
#define RPL_NOTOPIC 331
#define RPL_TOPIC 332
#define RPL_INVITING 341
#define RPL_SUMMONING 342
#define RPL_VERSION 351
#define RPL_WHOREPLY 352
#define RPL_ENDOFWHO 315
#define RPL_NAMREPLY 353
#define RPL_ENDOFNAMES 366
#define RPL_LINKS 364
#define RPL_ENDOFLINKS 365
#define RPL_BANLIST 367
#define RPL_ENDOFBANLIST 368
#define RPL_INFO 371
#define RPL_ENDOFINFO 374
#define RPL_MOTDSTART 375
#define RPL_MOTD 372
#define RPL_ENDOFMOTD 376
#define RPL_YOUREOPER 381
#define RPL_REHASHING 382
#define RPL_TIME 391
#define RPL_USERSSTART 392
#define RPL_USERS 393
#define RPL_ENDOFUSERS 394
#define RPL_NOUSERS 395
#define RPL_TRACELINK 200
#define RPL_TRACECONNECTING 201
#define RPL_TRACEHANDSHAKE 202
#define RPL_TRACEUNKNOWN 203
#define RPL_TRACEOPERATOR 204
#define RPL_TRACEUSER 205
#define RPL_TRACESERVER 206
#define RPL_TRACENEWTYPE 208
#define RPL_TRACELOG 261
#define RPL_STATSLINKINFO 211
#define RPL_STATSCOMMANDS 212
#define RPL_STATSCLINE 213
#define RPL_STATSNLINE 214
#define RPL_STATSILINE 215
#define RPL_STATSKLINE 216
#define RPL_STATSYLINE 218
#define RPL_ENDOFSTATS 219
#define RPL_STATSLLINE 241
#define RPL_STATSUPTIME 242
#define RPL_STATSOLINE 243
#define RPL_STATSHLINE 244
#define RPL_UMODEIS 221
#define RPL_LUSERCLIENT 251
#define RPL_LUSEROP 252
#define RPL_LUSERUNKNOWN 253
#define RPL_LUSERCHANNELS 254
#define RPL_LUSERME 255
#define RPL_ADMINME 256
#define RPL_ADMINLOC1 257
#define RPL_ADMINLOC2 258
#define RPL_ADMINEMAIL 259
#define RPL_WELCOME 001
#define RPL_YOURHOST 002
#define RPL_CREATED 003
#define RPL_MYINFO 004

static const char* replies[] = {
  /* 000 */ NULL,
  /* 001 */ ":%s 001 %s :Welcome to the %s Internet Relay Network %s!%s@%s\r\n",
  /* 002 */ ":%s 002 %s :Your host is %s, running version %s\r\n",
  /* 003 */ ":%s 003 %s :This server was created %s\r\n",
  /* 004 */ ":%s 004 %s :%s %s %s %s\r\n",
  /* 005 */ NULL,
  /* 006 */ NULL,
  /* 007 */ NULL,
  /* 008 */ NULL,
  /* 009 */ NULL,
  /* 010 */ NULL,
  /* 011 */ NULL,
  /* 012 */ NULL,
  /* 013 */ NULL,
  /* 014 */ NULL,
  /* 015 */ NULL,
  /* 016 */ NULL,
  /* 017 */ NULL,
  /* 018 */ NULL,
  /* 019 */ NULL,
  /* 020 */ NULL,
  /* 021 */ NULL,
  /* 022 */ NULL,
  /* 023 */ NULL,
  /* 024 */ NULL,
  /* 025 */ NULL,
  /* 026 */ NULL,
  /* 027 */ NULL,
  /* 028 */ NULL,
  /* 029 */ NULL,
  /* 030 */ NULL,
  /* 031 */ NULL,
  /* 032 */ NULL,
  /* 033 */ NULL,
  /* 034 */ NULL,
  /* 035 */ NULL,
  /* 036 */ NULL,
  /* 037 */ NULL,
  /* 038 */ NULL,
  /* 039 */ NULL,
  /* 040 */ NULL,
  /* 041 */ NULL,
  /* 042 */ NULL,
  /* 043 */ NULL,
  /* 044 */ NULL,
  /* 045 */ NULL,
  /* 046 */ NULL,
  /* 047 */ NULL,
  /* 048 */ NULL,
  /* 049 */ NULL,
  /* 050 */ NULL,
  /* 051 */ NULL,
  /* 052 */ NULL,
  /* 053 */ NULL,
  /* 054 */ NULL,
  /* 055 */ NULL,
  /* 056 */ NULL,
  /* 057 */ NULL,
  /* 058 */ NULL,
  /* 059 */ NULL,
  /* 060 */ NULL,
  /* 061 */ NULL,
  /* 062 */ NULL,
  /* 063 */ NULL,
  /* 064 */ NULL,
  /* 065 */ NULL,
  /* 066 */ NULL,
  /* 067 */ NULL,
  /* 068 */ NULL,
  /* 069 */ NULL,
  /* 070 */ NULL,
  /* 071 */ NULL,
  /* 072 */ NULL,
  /* 073 */ NULL,
  /* 074 */ NULL,
  /* 075 */ NULL,
  /* 076 */ NULL,
  /* 077 */ NULL,
  /* 078 */ NULL,
  /* 079 */ NULL,
  /* 080 */ NULL,
  /* 081 */ NULL,
  /* 082 */ NULL,
  /* 083 */ NULL,
  /* 084 */ NULL,
  /* 085 */ NULL,
  /* 086 */ NULL,
  /* 087 */ NULL,
  /* 088 */ NULL,
  /* 089 */ NULL,
  /* 090 */ NULL,
  /* 091 */ NULL,
  /* 092 */ NULL,
  /* 093 */ NULL,
  /* 094 */ NULL,
  /* 095 */ NULL,
  /* 096 */ NULL,
  /* 097 */ NULL,
  /* 098 */ NULL,
  /* 099 */ NULL,
  /* 100 */ NULL,
  /* 101 */ NULL,
  /* 102 */ NULL,
  /* 103 */ NULL,
  /* 104 */ NULL,
  /* 105 */ NULL,
  /* 106 */ NULL,
  /* 107 */ NULL,
  /* 108 */ NULL,
  /* 109 */ NULL,
  /* 110 */ NULL,
  /* 111 */ NULL,
  /* 112 */ NULL,
  /* 113 */ NULL,
  /* 114 */ NULL,
  /* 115 */ NULL,
  /* 116 */ NULL,
  /* 117 */ NULL,
  /* 118 */ NULL,
  /* 119 */ NULL,
  /* 120 */ NULL,
  /* 121 */ NULL,
  /* 122 */ NULL,
  /* 123 */ NULL,
  /* 124 */ NULL,
  /* 125 */ NULL,
  /* 126 */ NULL,
  /* 127 */ NULL,
  /* 128 */ NULL,
  /* 129 */ NULL,
  /* 130 */ NULL,
  /* 131 */ NULL,
  /* 132 */ NULL,
  /* 133 */ NULL,
  /* 134 */ NULL,
  /* 135 */ NULL,
  /* 136 */ NULL,
  /* 137 */ NULL,
  /* 138 */ NULL,
  /* 139 */ NULL,
  /* 140 */ NULL,
  /* 141 */ NULL,
  /* 142 */ NULL,
  /* 143 */ NULL,
  /* 144 */ NULL,
  /* 145 */ NULL,
  /* 146 */ NULL,
  /* 147 */ NULL,
  /* 148 */ NULL,
  /* 149 */ NULL,
  /* 150 */ NULL,
  /* 151 */ NULL,
  /* 152 */ NULL,
  /* 153 */ NULL,
  /* 154 */ NULL,
  /* 155 */ NULL,
  /* 156 */ NULL,
  /* 157 */ NULL,
  /* 158 */ NULL,
  /* 159 */ NULL,
  /* 160 */ NULL,
  /* 161 */ NULL,
  /* 162 */ NULL,
  /* 163 */ NULL,
  /* 164 */ NULL,
  /* 165 */ NULL,
  /* 166 */ NULL,
  /* 167 */ NULL,
  /* 168 */ NULL,
  /* 169 */ NULL,
  /* 170 */ NULL,
  /* 171 */ NULL,
  /* 172 */ NULL,
  /* 173 */ NULL,
  /* 174 */ NULL,
  /* 175 */ NULL,
  /* 176 */ NULL,
  /* 177 */ NULL,
  /* 178 */ NULL,
  /* 179 */ NULL,
  /* 180 */ NULL,
  /* 181 */ NULL,
  /* 182 */ NULL,
  /* 183 */ NULL,
  /* 184 */ NULL,
  /* 185 */ NULL,
  /* 186 */ NULL,
  /* 187 */ NULL,
  /* 188 */ NULL,
  /* 189 */ NULL,
  /* 190 */ NULL,
  /* 191 */ NULL,
  /* 192 */ NULL,
  /* 193 */ NULL,
  /* 194 */ NULL,
  /* 195 */ NULL,
  /* 196 */ NULL,
  /* 197 */ NULL,
  /* 198 */ NULL,
  /* 199 */ NULL,
  /* 200 */ NULL,
  /* 201 */ NULL,
  /* 202 */ NULL,
  /* 203 */ NULL,
  /* 204 */ NULL,
  /* 205 */ NULL,
  /* 206 */ NULL,
  /* 207 */ NULL,
  /* 208 */ NULL,
  /* 209 */ NULL,
  /* 210 */ NULL,
  /* 211 */ NULL,
  /* 212 */ NULL,
  /* 213 */ NULL,
  /* 214 */ NULL,
  /* 215 */ NULL,
  /* 216 */ NULL,
  /* 217 */ NULL,
  /* 218 */ NULL,
  /* 219 */ NULL,
  /* 220 */ NULL,
  /* 221 */ NULL,
  /* 222 */ NULL,
  /* 223 */ NULL,
  /* 224 */ NULL,
  /* 225 */ NULL,
  /* 226 */ NULL,
  /* 227 */ NULL,
  /* 228 */ NULL,
  /* 229 */ NULL,
  /* 230 */ NULL,
  /* 231 */ NULL,
  /* 232 */ NULL,
  /* 233 */ NULL,
  /* 234 */ NULL,
  /* 235 */ NULL,
  /* 236 */ NULL,
  /* 237 */ NULL,
  /* 238 */ NULL,
  /* 239 */ NULL,
  /* 240 */ NULL,
  /* 241 */ NULL,
  /* 242 */ NULL,
  /* 243 */ NULL,
  /* 244 */ NULL,
  /* 245 */ NULL,
  /* 246 */ NULL,
  /* 247 */ NULL,
  /* 248 */ NULL,
  /* 249 */ NULL,
  /* 250 */ NULL,
  /* 251 */ NULL,
  /* 252 */ NULL,
  /* 253 */ NULL,
  /* 254 */ NULL,
  /* 255 */ NULL,
  /* 256 */ NULL,
  /* 257 */ NULL,
  /* 258 */ NULL,
  /* 259 */ NULL,
  /* 260 */ NULL,
  /* 261 */ NULL,
  /* 262 */ NULL,
  /* 263 */ NULL,
  /* 264 */ NULL,
  /* 265 */ NULL,
  /* 266 */ NULL,
  /* 267 */ NULL,
  /* 268 */ NULL,
  /* 269 */ NULL,
  /* 270 */ NULL,
  /* 271 */ NULL,
  /* 272 */ NULL,
  /* 273 */ NULL,
  /* 274 */ NULL,
  /* 275 */ NULL,
  /* 276 */ NULL,
  /* 277 */ NULL,
  /* 278 */ NULL,
  /* 279 */ NULL,
  /* 280 */ NULL,
  /* 281 */ NULL,
  /* 282 */ NULL,
  /* 283 */ NULL,
  /* 284 */ NULL,
  /* 285 */ NULL,
  /* 286 */ NULL,
  /* 287 */ NULL,
  /* 288 */ NULL,
  /* 289 */ NULL,
  /* 290 */ NULL,
  /* 291 */ NULL,
  /* 292 */ NULL,
  /* 293 */ NULL,
  /* 294 */ NULL,
  /* 295 */ NULL,
  /* 296 */ NULL,
  /* 297 */ NULL,
  /* 298 */ NULL,
  /* 299 */ NULL,
  /* 300 */ NULL,
  /* 301 */ NULL,
  /* 302 */ NULL,
  /* 303 */ NULL,
  /* 304 */ NULL,
  /* 305 */ NULL,
  /* 306 */ NULL,
  /* 307 */ NULL,
  /* 308 */ NULL,
  /* 309 */ NULL,
  /* 310 */ NULL,
  /* 311 */ NULL,
  /* 312 */ NULL,
  /* 313 */ NULL,
  /* 314 */ NULL,
  /* 315 */ NULL,
  /* 316 */ NULL,
  /* 317 */ NULL,
  /* 318 */ NULL,
  /* 319 */ NULL,
  /* 320 */ NULL,
  /* 321 */ ":%s 321 %s Channel :Users  Name\r\n",
  /* 322 */ ":%s 322 %s %s %s :%s\r\n",
  /* 323 */ ":%s 323 %s :End of /LIST\r\n",
  /* 324 */ NULL,
  /* 325 */ NULL,
  /* 326 */ NULL,
  /* 327 */ NULL,
  /* 328 */ NULL,
  /* 329 */ NULL,
  /* 330 */ NULL,
  /* 331 */ NULL,
  /* 332 */ ":%s 332 %s %s :%s\r\n",
  /* 333 */ NULL,
  /* 334 */ NULL,
  /* 335 */ NULL,
  /* 336 */ NULL,
  /* 337 */ NULL,
  /* 338 */ NULL,
  /* 339 */ NULL,
  /* 340 */ NULL,
  /* 341 */ NULL,
  /* 342 */ NULL,
  /* 343 */ NULL,
  /* 344 */ NULL,
  /* 345 */ NULL,
  /* 346 */ NULL,
  /* 347 */ NULL,
  /* 348 */ NULL,
  /* 349 */ NULL,
  /* 350 */ NULL,
  /* 351 */ NULL,
  /* 352 */ NULL,
  /* 353 */ ":%s 353 %s = %s :%s\r\n",
  /* 354 */ NULL,
  /* 355 */ NULL,
  /* 356 */ NULL,
  /* 357 */ NULL,
  /* 358 */ NULL,
  /* 359 */ NULL,
  /* 360 */ NULL,
  /* 361 */ NULL,
  /* 362 */ NULL,
  /* 363 */ NULL,
  /* 364 */ NULL,
  /* 365 */ NULL,
  /* 366 */ ":%s 366 %s %s :End of /NAMES list\r\n",
  /* 367 */ NULL,
  /* 368 */ NULL,
  /* 369 */ NULL,
  /* 370 */ NULL,
  /* 371 */ NULL,
  /* 372 */ NULL,
  /* 373 */ NULL,
  /* 374 */ NULL,
  /* 375 */ NULL,
  /* 376 */ NULL,
  /* 377 */ NULL,
  /* 378 */ NULL,
  /* 379 */ NULL,
  /* 380 */ NULL,
  /* 381 */ NULL,
  /* 382 */ NULL,
  /* 383 */ NULL,
  /* 384 */ NULL,
  /* 385 */ NULL,
  /* 386 */ NULL,
  /* 387 */ NULL,
  /* 388 */ NULL,
  /* 389 */ NULL,
  /* 390 */ NULL,
  /* 391 */ NULL,
  /* 392 */ NULL,
  /* 393 */ NULL,
  /* 394 */ NULL,
  /* 395 */ NULL,
  /* 396 */ NULL,
  /* 397 */ NULL,
  /* 398 */ NULL,
  /* 399 */ NULL,
  /* 400 */ NULL,
  /* 401 */ ":%s 401 %s %s :No such nick/channel\r\n",
  /* 402 */ NULL,
  /* 403 */ ":%s 403 %s %s :No such channel\r\n",
  /* 404 */ NULL,
  /* 405 */ NULL,
  /* 406 */ NULL,
  /* 407 */ NULL,
  /* 408 */ NULL,
  /* 409 */ NULL,
  /* 410 */ NULL,
  /* 411 */ NULL,
  /* 412 */ NULL,
  /* 413 */ NULL,
  /* 414 */ NULL,
  /* 415 */ NULL,
  /* 416 */ NULL,
  /* 417 */ NULL,
  /* 418 */ NULL,
  /* 419 */ NULL,
  /* 420 */ NULL,
  /* 421 */ ":%s 421 %s %s :Unknown command\r\n",
  /* 422 */ NULL,
  /* 423 */ NULL,
  /* 424 */ NULL,
  /* 425 */ NULL,
  /* 426 */ NULL,
  /* 427 */ NULL,
  /* 428 */ NULL,
  /* 429 */ NULL,
  /* 430 */ NULL,
  /* 431 */ NULL,
  /* 432 */ NULL,
  /* 433 */ NULL,
  /* 434 */ NULL,
  /* 435 */ NULL,
  /* 436 */ NULL,
  /* 437 */ NULL,
  /* 438 */ NULL,
  /* 439 */ NULL,
  /* 440 */ NULL,
  /* 441 */ NULL,
  /* 442 */ ":%s 442 %s %s :You're not on that channel\r\n",
  /* 443 */ NULL,
  /* 444 */ NULL,
  /* 445 */ NULL,
  /* 446 */ NULL,
  /* 447 */ NULL,
  /* 448 */ NULL,
  /* 449 */ NULL,
  /* 450 */ NULL,
  /* 451 */ ":%s 451 %s :You have not registered\r\n",
  /* 452 */ NULL,
  /* 453 */ NULL,
  /* 454 */ NULL,
  /* 455 */ NULL,
  /* 456 */ NULL,
  /* 457 */ NULL,
  /* 458 */ NULL,
  /* 459 */ NULL,
  /* 460 */ NULL,
  /* 461 */ ":%s 461 %s %s :Not enough parameters\r\n",
  /* 462 */ ":%s 462 %s :You may not reregister\r\n",
  /* 463 */ NULL,
  /* 464 */ NULL,
  /* 465 */ NULL,
  /* 466 */ NULL,
  /* 467 */ NULL,
  /* 468 */ NULL,
  /* 469 */ NULL,
  /* 470 */ NULL,
  /* 471 */ NULL,
  /* 472 */ NULL,
  /* 473 */ NULL,
  /* 474 */ NULL,
  /* 475 */ NULL,
  /* 476 */ NULL,
  /* 477 */ NULL,
  /* 478 */ NULL,
  /* 479 */ NULL,
  /* 480 */ NULL,
  /* 481 */ NULL,
  /* 482 */ NULL,
  /* 483 */ NULL,
  /* 484 */ NULL,
  /* 485 */ NULL,
  /* 486 */ NULL,
  /* 487 */ NULL,
  /* 488 */ NULL,
  /* 489 */ NULL,
  /* 490 */ NULL,
  /* 491 */ NULL,
  /* 492 */ NULL,
  /* 493 */ NULL,
  /* 494 */ NULL,
  /* 495 */ NULL,
  /* 496 */ NULL,
  /* 497 */ NULL,
  /* 498 */ NULL,
  /* 499 */ NULL,
  /* 500 */ NULL,
  /* 501 */ NULL,
  /* 502 */ NULL
};

#endif /* REPLIES_H */
