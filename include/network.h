#ifndef NETWORK_H
#define NETWORK_H

/* TODO: Put this params into a separeted .h file */
#define LISTENQ 1
#define MAXDATASIZE 100


/* TODO: Put this ERRORS macros into a separeted errors.h file */
#define SOCKET_FAILURE 2
#define BIND_FAILURE 3
#define LISTEN_FAILURE 4
#define CONNECT_FAILURE 5

int create_socket(struct sockaddr_in* server, char* send_buffer);
void verify_socket_creation(int listen_id);
void configure_server_socket(struct sockaddr_in* server, int listen_id, int port_number);
void verify_socket_bind(struct sockaddr_in* server, int listen_id);
void listen_connections(int listen_id, int maximum_number_of_clients);
int get_connection(int listen_id);

#endif /* NETWORK_H */
