#ifndef __WEATHER_H__
#define __WEATHER_H__

int parse_temperature(char *weather_xml);
int get_raw_weather(char *server_reply);

#endif
