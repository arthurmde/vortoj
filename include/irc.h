#ifndef IRC_H
#define IRC_H

#include "client.h"
#include "channel.h"
#include "list.h"


typedef struct _Server
{
  List* channels;
  Client** clients;
  char* name;
}Server;

int maximum_number_of_clients;
extern Server* create_server(char* name);
extern int free_server(Server* server);
extern void welcome_message();
extern void handle_action(char *command, Client* client, Server* server);
extern void send_quit_message(Client* client, char* message);
extern void quit(char *command, Client* client, Server* server);
extern bool equal(const char* first, const char* second);
extern void registration(char* command_name, char *command, Client* client, Server* server);
extern void join(Server* server, char* command_name, Client *client);
extern void part(Server* server, char* command_name, Client *client);
extern void list(Server* server, char* command_name, Client* client);
extern char* basic_response(Server* server, int answer);
extern int create_new_channel(Server* server, char* name, char* topic);
extern int print_channels(Server* server);
extern bool verify_irc_message(char* receive_buffer, int size);
extern void remove_special_chars(char* receive_buffer, int size);
extern void macdata(char* command, Client* client);
extern void machora(char* command, Client* client);
extern void mactemperatura(char* command, Client* client);
extern void unknown_command(char* command_name, Client* client);
extern void privmsg(Server* server, char* command, Client* client);
extern Client* get_user_by_nick(Server* server, char* nick);


#endif /* IRC_H */
