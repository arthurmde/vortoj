#ifndef LIST_H
#define LIST_H

#include "node.h"
#include "client.h"

#define ERROR_NULL_PARAMETER -1
#define ERROR_EMPTY_LIST -2
#define OK 0
#define ERROR_OUT_OF_MEMORY -3
#define ERROR_INVALID_PARAMETER -4

typedef struct _List
{
	Node *head;
	Node *tail;
}List;

extern List *create_list();
extern int free_list(List *list);
extern int initialize_list(List *list);
extern int empty_list(List *list);
extern int push_front(List *list, void *data);
extern int pop_back(List *list);
extern int push_back(List *list, void *data);
extern int pop_front(List *list);
extern List *clone_list(List* list);
extern int size_list(List *list);
extern int element_list(void *data, List *list, int position);
extern int push (List *list, void *data, int position);
extern int pop(List *list, int position);

extern int copy_list(List *original, List *copy);

extern int clear_list(List *list);


#endif
