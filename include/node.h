#ifndef NODE_H
#define NODE_H

typedef struct _Node{
	void *data;
	struct _Node * next;
	struct _Node * prev;
} Node;

extern Node * create_node();

extern int free_node(Node *n);

#endif
